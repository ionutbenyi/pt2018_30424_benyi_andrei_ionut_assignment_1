package Assignment1.PolynomialsProject;

public class Monomial {
	double coeff;
	public int power;
	
	public Monomial(double a, int b) {
		this.coeff=a;
		this.power=b;
	}
	
	public double getPower() {
		return this.power;
	}
	
	public void setPower(int a) {
		this.power=a;
	}
	
	public double getCoeff() {
		return this.coeff;
	}
	
	public void setCoeff(int a) {
		this.coeff=a;
	}
	
}

