package Assignment1.PolynomialsProject;

import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.*;

import javax.swing.*;
public class PolView extends JFrame{
	
	private JFrame mainFr;
	private JLabel headerLabel;
	private JLabel inputLabel;
	private JPanel cp1;
	private JPanel cp2;
	private JPanel cp3;
	private JPanel cp4;
	private JPanel cp5;
	private JPanel cp6;
	private JPanel cp7;
	private JPanel cp8;
	private JPanel cp12;
	private JPanel cp22;
	private JPanel cp32;
	private JPanel cp42;
	private JLabel degLabel;
	private JLabel degLabel2;
	public JTextField degTextf;
	public JTextField degTextf2;
	public JLabel powerLabel;
	private JLabel coeffLabel;
	public JLabel powerLabel2;
	private JLabel coeffLabel2;
	public JTextField coeffTextf;
	public JTextField coeffTextf2;
	private JButton setElButton;
	private JButton setElButton2;
	private JLabel inputPolLabel;
	public JTextField inputPolTextf;
	private JLabel inputPolLabel2;
	public JTextField inputPolTextf2;
	private JButton addButton;
	private JButton subButton;
	private JButton mulButton;
	private JButton divButton;
	private JButton derButton;
	private JButton intButton;
	private JLabel outputPolLabel;
	public JTextField outputPolTextf;
	private JButton resetButton;
	
	private PolModel m_model;
	
	PolView(PolModel model){
		m_model=model;
		
		mainFr=new JFrame("Polynomial ops");
		mainFr.setSize(1030,250);
	
		mainFr.setLayout(new FlowLayout());
		
		degLabel=new JLabel("Degree:");
		powerLabel=new JLabel("Power 0:");
		coeffLabel=new JLabel("Coeff:");
		inputPolLabel=new JLabel("Input polynomial:");
		degLabel2=new JLabel("Degree 2:");
		powerLabel2=new JLabel("Power 0:");
		coeffLabel2=new JLabel("Coeff 2:");
		inputPolLabel2=new JLabel("Input polynomial 2:");
		outputPolLabel=new JLabel("Output polynomial:");
		
		degTextf=new JTextField(5);
		coeffTextf=new JTextField(5);
		inputPolTextf=new JTextField(40);
		degTextf2=new JTextField(5);
		coeffTextf2=new JTextField(5);
		inputPolTextf2=new JTextField(40);
		outputPolTextf=new JTextField(80);
		
		setElButton=new JButton("Set");
		setElButton2=new JButton("Set 2");
		addButton=new JButton(" + ");
		subButton=new JButton(" - ");
		mulButton=new JButton(" * ");
		divButton=new JButton(" / ");
		derButton=new JButton(" f'(x) ");
		intButton=new JButton(" F(x) ");
		resetButton=new JButton(" Reset ");
		
		mainFr.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
		
		cp1=new JPanel();
		cp1.setLayout(new FlowLayout());
		mainFr.add(cp1);
		
		cp2=new JPanel();
		cp2.setLayout(new FlowLayout());
		mainFr.add(cp2);
		
		cp3=new JPanel();
		cp3.setLayout(new FlowLayout());
		mainFr.add(cp3);
		
		cp4=new JPanel();
		cp4.setLayout(new FlowLayout());
		mainFr.add(cp4);
		
		cp12=new JPanel();
		cp12.setLayout(new FlowLayout());
		mainFr.add(cp12);
		
		cp22=new JPanel();
		cp22.setLayout(new FlowLayout());
		mainFr.add(cp22);
		
		cp32=new JPanel();
		cp32.setLayout(new FlowLayout());
		mainFr.add(cp32);
		
		cp42=new JPanel();
		cp42.setLayout(new FlowLayout());
		mainFr.add(cp42);
		
		cp5=new JPanel();
		cp5.setLayout(new FlowLayout());
		mainFr.add(cp5);
		
		cp6=new JPanel();
		cp6.setLayout(new FlowLayout());
		mainFr.add(cp6);
		
		cp7=new JPanel();
		cp7.setLayout(new FlowLayout());
		mainFr.add(cp7);
		
		cp8=new JPanel();
		cp8.setLayout(new FlowLayout());
		mainFr.add(cp8);
		
		cp1.add(degLabel); cp1.add(degTextf);
		cp2.add(powerLabel);
		cp3.add(coeffLabel); cp3.add(coeffTextf); cp3.add(setElButton);
		cp4.add(inputPolLabel); cp4.add(inputPolTextf);
		cp12.add(degLabel2); cp12.add(degTextf2);
		cp22.add(powerLabel2);
		cp32.add(coeffLabel2); cp32.add(coeffTextf2); cp32.add(setElButton2);
		cp42.add(inputPolLabel2); cp42.add(inputPolTextf2);
		cp5.add(addButton); cp5.add(subButton); cp5.add(mulButton);
		cp6.add(divButton); cp6.add(derButton); cp6.add(intButton);
		cp7.add(outputPolLabel); cp7.add(outputPolTextf);
		cp8.add(resetButton);
		
		mainFr.setVisible(true);
		
	}
	
	String getUserInputDegree1() {
		return degTextf.getText();
	}
	
	String getUserInputDegree2() {
		return degTextf2.getText();
	}
	
	String getUserInputCoeff1() {
		return coeffTextf.getText();
	}
	
	String getUserInputCoeff2() {
		return coeffTextf.getText();
	}
	
	void setInputPol1(Polynomial rez) {
		String r=new String();
		for(int i=0;i<rez.pol.size();i++) {
			r.concat("+"+(rez.pol.get(i)).coeff+"x^"+(rez.pol.get(i)).power);
		}
		inputPolTextf.setText(r);
	}
	
	void setInputPol2(Polynomial rez) {
		String r=new String();
		for(int i=0;i<rez.pol.size();i++) {
			r.concat("+"+(rez.pol.get(i)).coeff+"x^"+(rez.pol.get(i)).power);
		}
		inputPolTextf2.setText(r);
	}
	
	void setPower1(int p) {
		String r=new String();
		r.concat("Power "+p+":");
		powerLabel.setText(r);
	}
	
	void setPower2(int p) {
		String r=new String();
		r.concat("Power "+p+":");
		powerLabel2.setText(r);
	}
	
	void setTotal(Polynomial rez) {
		String r=new String();
		for(int i=0;i<rez.pol.size();i++) {
			r.concat(" + "+(rez.pol.get(i)).coeff+"x^"+(rez.pol.get(i)).power);
		}
		outputPolTextf.setText(r);
	}
	
	void addSet1Listener(ActionListener set1al) {
		setElButton.addActionListener(set1al);
	}
	
	void addSet2Listener(ActionListener set2al) {
		setElButton2.addActionListener(set2al);
	}
	
	void addAddListener(ActionListener addal) {
		addButton.addActionListener(addal);
	}
	
	void addSubListener(ActionListener subal) {
		subButton.addActionListener(subal);
	}
	
	void addMulListener(ActionListener mulal) {
		mulButton.addActionListener(mulal);
	}
	
	void addDivListener(ActionListener dival) {
		divButton.addActionListener(dival);
	}
	
	void addDerListener(ActionListener deral) {
		derButton.addActionListener(deral);
	}
	
	void addIntListener(ActionListener intal) {
		intButton.addActionListener(intal);
	}
	
	void addResetListener(ActionListener resal) {
		resetButton.addActionListener(resal);
	}

}
