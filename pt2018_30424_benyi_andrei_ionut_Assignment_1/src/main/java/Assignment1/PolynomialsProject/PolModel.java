package Assignment1.PolynomialsProject;

public class PolModel {
	public Polynomial rez=new Polynomial(0);
	public Polynomial p1=new Polynomial(0);
	public Polynomial p2=new Polynomial(0);
	public void reset() {
		rez=new Polynomial(0);
	}
	
	PolModel(){
		reset();
	}
	
	public void multiply(Polynomial p1, Polynomial p2) {
		rez=p1.multiplicationPol(p2);
	}
	
	public void add(Polynomial p1, Polynomial p2) {
		rez=p1.additionPol(p2);
	}
	
	public void subtract(Polynomial p1, Polynomial p2) {
		rez=p1.subtractionPol(p2);
	}
	
	public void divide(Polynomial p1, Polynomial p2) {
		rez=p1.divisionPol(p2);
	}
	
	public void derivate(Polynomial p1) {
		rez=p1.differentiatePol();
	}
	
	public void integrate(Polynomial p1) {
		rez=p1.integratePol();
	}
	
}
