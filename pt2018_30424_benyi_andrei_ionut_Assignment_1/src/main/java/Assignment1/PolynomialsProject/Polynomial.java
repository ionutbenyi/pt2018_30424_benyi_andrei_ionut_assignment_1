package Assignment1.PolynomialsProject;

import java.util.*;
import java.awt.*;
import java.io.*;
import java.math.*;

public class Polynomial {
	int degree;
	ArrayList <Monomial> pol=new ArrayList<Monomial>();
	
	public Polynomial(int d) {		//constructor using the degree only
		this.degree=d;
	}
	
	public void addMon(Monomial m) {
		this.pol.add(m);
	}
	
	public Polynomial additionPol(Polynomial t) {
		int m=Math.max(this.pol.size(), t.pol.size());
		int min =Math.min(this.pol.size(),t.pol.size());
		Polynomial a=this;
		Polynomial b=new Polynomial(m);
		ArrayList <Monomial> trm=new ArrayList<Monomial>(m);
		
		for(int i=0;i<min;i++) {
			double t1,t2;
			t1=(a.pol.get(i)).coeff;
			t2=(t.pol.get(i)).coeff;
			trm.add(new Monomial((t1+t2),i));
		}	//adding the common elements - until the minimum degree
		
		//adding the rest
		if(a.pol.size()==m) {
			for(int i=min;i<m;i++) {
				trm.add(a.pol.get(i));
			}
		}
		
		else {
			for(int i=min;i<m;i++) {
				trm.add(t.pol.get(i));
			}
		}
		
		b.pol=trm;
		b.degree=m;
		return b;
	}
	
	public Polynomial subtractionPol(Polynomial t) {	//analogue work as in the case of the addition
		int m=Math.max(this.pol.size(), t.pol.size());
		int min =Math.min(this.pol.size(),t.pol.size());
		Polynomial a=this;
		Polynomial b=new Polynomial(m);
		ArrayList <Monomial> trm=new ArrayList<Monomial>(m);
		
		for(int i=0;i<min;i++) {
			double t1,t2;
			t1=(a.pol.get(i)).coeff;
			t2=(t.pol.get(i)).coeff;
			trm.add(new Monomial((t1-t2),i));
		}
		
		if(a.pol.size()==m) {
			for(int i=min;i<m;i++) {
				trm.add(a.pol.get(i));
			}
		}
		
		else {
			for(int i=min;i<m;i++) {
				trm.add(t.pol.get(i));
			}
		}
		
		b.pol=trm;
		b.degree=m;
		return b;
	}
	
	public Polynomial multiplicationPol(Polynomial t) {
		Polynomial a=this;
		int m=t.degree+a.degree;
		Polynomial b=new Polynomial(m);
		ArrayList <Monomial> trm=new ArrayList<Monomial>(m);
		for(int i=0;i<=m;i++) {
			trm.add(new Monomial(0,i));
		}
		//multiply each element with each other
		for(int i=0;i<a.pol.size();i++) {
			for(int j=0;j<t.pol.size();j++) {
				double auxr=((a.pol.get(i)).coeff)*((t.pol.get(j)).coeff);
				(trm.get(i+j)).coeff=(trm.get(i+j)).coeff+auxr;
			}
		}
		
		b.pol=trm;
		return b;
	}
	
	//mathod for converting a monomial into a single-term polynomial
	public Polynomial monToPol(Monomial m) {
		int deg=m.power;
		Polynomial r=new Polynomial(deg);
		for(int i=0;i<deg;i++) {
			r.addMon(new Monomial(0,i));
		}
		r.addMon(m);
		return r;
	}

	//method for dividing two monomials
	public Monomial divMon(Monomial m1, Monomial m2) {
		int powerm=m1.power-m2.power;
		double coeffm=m1.coeff/m2.coeff;
		Monomial result=new Monomial(coeffm,powerm);
		return result;
	}
	
	public Polynomial divisionPol(Polynomial t) {
		Polynomial a=this;
		ArrayList<Monomial>resList=new ArrayList<Monomial>();
		int dg=a.degree;
		Polynomial nwRef=a;
		int ct=a.degree-t.degree;
		if(this.degree<t.degree)
			return new Polynomial(0);
		else {
			while(ct>=0) {
				Monomial m=divMon(nwRef.pol.get(dg),t.pol.get(t.pol.size()-1));
				resList.add(m);
				Polynomial prd=(monToPol(m)).multiplicationPol(t);
				Polynomial dif=nwRef.subtractionPol(prd);
				nwRef=dif;
				dg--;
				ct--;
			}
			Polynomial p=new Polynomial(a.degree-t.degree);
			p.pol=resList;
			return p;
		}
	}
	
	public Polynomial differentiatePol() {
		Polynomial p=this;
		for(int i=1;i<p.pol.size();i++) {
			(p.pol.get(i)).coeff=((p.pol.get(i)).coeff)*i;
			(p.pol.get(i)).power--;
		}
		p.degree--;
		p.pol.remove(0);
		return p;
	}

	
	public Polynomial integratePol() {
		Polynomial p=this;
		Polynomial r=new Polynomial(p.degree+1);
		for(int i=0;i<p.pol.size()+1;i++){
			r.addMon(new Monomial(1,i));
		}
		r.pol.get(0).coeff=0;
		for(int i=0;i<p.pol.size();i++) {
			r.pol.get(i+1).coeff=(p.pol.get(i).coeff)/((p.pol.get(i).power)+1);
		}
		return r;
	}
	
	public String printPol() {
		Polynomial p=this;
		String t=new String("");
		t=t+p.pol.get(0).coeff;
		for(int i=1;i<p.pol.size();i++) {
			if(p.pol.get(i).coeff>0) {
				t=t+" + ";
			}
			if(p.pol.get(i).coeff==0) {
				t=t;
			}
			if(p.pol.get(i).coeff==1) {
				t=t+"x^"+i;
			}
			if(p.pol.get(i).coeff!=0 && p.pol.get(i).coeff!=1) {
				t=t+p.pol.get(i).coeff+"x^"+i;
			}
		}
		return t;
	}
	
}
