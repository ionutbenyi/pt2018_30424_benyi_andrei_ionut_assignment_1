package Assignment1.PolynomialsProject;

public class PolOp {

	public Polynomial addPT(Polynomial p1, Polynomial p2) {
		return p1.additionPol(p2);
	}
	
	public Polynomial subPT(Polynomial p1, Polynomial p2) {
		return p1.subtractionPol(p2);
	}
	
	public Polynomial mulPT(Polynomial p1, Polynomial p2) {
		return p1.multiplicationPol(p2);
	}
	
	public Polynomial derPT(Polynomial p1) {
		return p1.differentiatePol();
	}
	
	public Polynomial intPT(Polynomial p1) {
		return p1.integratePol();
	}
}
