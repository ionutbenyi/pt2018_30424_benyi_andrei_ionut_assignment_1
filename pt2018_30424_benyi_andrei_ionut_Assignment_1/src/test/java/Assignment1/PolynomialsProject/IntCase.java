package Assignment1.PolynomialsProject;

import static org.junit.Assert.*;

import org.junit.Test;

public class IntCase {

	@Test
	public void test() {
		PolOp test=new PolOp();
		Polynomial rez=new Polynomial(3);
		rez.addMon(new Monomial(0,0));
		rez.addMon(new Monomial(0,1));
		rez.addMon(new Monomial(0,2));
		rez.addMon(new Monomial(1,3));
		
		Polynomial p1=new Polynomial(2);
		p1.addMon(new Monomial(0,0));
		p1.addMon(new Monomial(0,1));
		p1.addMon(new Monomial(3,2));
	
		Polynomial pr=test.intPT(p1);
		
		for(int i=0;i<=3;i++) {
			assertEquals(pr.pol.get(i).coeff,rez.pol.get(i).coeff,0);
		}
	}

}
