package Assignment1.PolynomialsProject;

import static org.junit.Assert.*;

import org.junit.Test;

public class AddCase {

	@Test
	public void testAdd() {
		PolOp test=new PolOp();
		Polynomial rez=new Polynomial(1);
		rez.addMon(new Monomial(4,0));
		rez.addMon(new Monomial(2,1));
		
		Polynomial p1=new Polynomial(1);
		p1.addMon(new Monomial(1,0));
		p1.addMon(new Monomial(2,1));
		
		Polynomial p2=new Polynomial(0);
		p2.addMon(new Monomial(3,0));
		
		Polynomial pr=test.addPT(p1, p2);
		
		for(int i=0;i<=1;i++) {
			assertEquals(pr.pol.get(i).coeff,rez.pol.get(i).coeff,0);
		}
	}

}
